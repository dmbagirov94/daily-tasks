#!/bin/sh

docker-compose --env-file=src/.env run php -t \
&& cd ./src \
&& php artisan test
