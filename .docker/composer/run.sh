#!/bin/bash
sh ../.docker/wait-for.sh db:${DB_PORT}
composer install --ignore-platform-reqs
exec $@
