#!/bin/bash
sh ../.docker/wait-for.sh db:${DB_PORT}
php artisan migrate --seed
php artisan queue:work --daemon redis &
exec $@
