<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserTaskController;
use Illuminate\Support\Facades\Route;

Route::middleware(['api'])->group(function () {
    Route::prefix('my-tasks')->group(function () {
        Route::get('', UserTaskController::class . '@index');
        Route::prefix('{userTask}')->group( function () {
            Route::patch('complete', UserTaskController::class . '@complete');
            Route::patch('replace', UserTaskController::class . '@replace');
        });
    });
});

Route::post('registration', AuthController::class . '@registration');
Route::post('login', AuthController::class . '@login');
