<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'category_id',
        'title',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function userTasks()
    {
        return $this->hasMany(UserTask::class);
    }
}
