<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\User;
use App\Services\UserService;
use App\Services\UserTaskService;
use Illuminate\Console\Command;

class UserTasksGenerationCommand extends Command
{
    protected $signature = 'user-task:generate-for-today';

    protected $description = 'Generate tasks selection for today';
    private UserService $userService;
    private UserTaskService $userTaskService;

    public function __construct(UserService $userService, UserTaskService $taskService)
    {
        parent::__construct();
        $this->userService = $userService;
        $this->userTaskService = $taskService;
    }

    public function info($string, $verbosity = null)
    {
        $now = now()->format('H:i:s');
        $string = "[$now]: $string";

        parent::info($string, $verbosity = null);
    }

    public function handle()
    {
        $this->info('Starting task selection generation');
        $categories = Category::all();
        $date = now();
        $this->userService
            ->list()
            ->each(
                fn(User $user) => $this->userTaskService->generateTaskSelection($user, $categories, $date)
            );
        $this->info('Task selection generation has completed');

        return 0;
    }
}
