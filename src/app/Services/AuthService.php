<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;

class AuthService
{
    private UserService $userService;
    private JWT $jwt;

    public function __construct(UserService $userService, JWT $jwt)
    {
        $this->userService = $userService;
        $this->jwt = $jwt;
    }

    public function registration($data): User
    {
        $data['password'] = Hash::make($data['password']);

        return $this->userService->create($data);
    }

    public function login($data)
    {
        return auth()->attempt($data);
    }

    /**
     * Generate an authentication token for a given user
     *
     * @param  User  $user
     *
     * @return string
     */
    public function authFromUser(User $user): string
    {
        return $this->jwt->fromUser($user);
    }

    public function logout(): void
    {
        auth()->logout();
    }

    public function currentUser(): ?Authenticatable
    {
        return auth()->user();
    }
}
