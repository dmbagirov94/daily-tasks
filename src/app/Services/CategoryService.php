<?php

namespace App\Services;

use App\Repositories\Interfaces\CategoryRepositoryInterface;
use Illuminate\Support\Collection;

class CategoryService
{
    private CategoryRepositoryInterface $repository;

    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function list($where = null): Collection
    {
        return $this->repository->list($where);
    }

    public function create($data)
    {
        return $this->repository->create($data);
    }
}
