<?php

namespace App\Services;

use App\Models\Task;
use App\Models\User;
use App\Models\UserTask;
use App\Repositories\Interfaces\UserTaskRepositoryInterface;
use Illuminate\Support\Collection;

class UserTaskService
{
    private UserTaskRepositoryInterface $repository;
    private TaskService $taskService;

    public function __construct(UserTaskRepositoryInterface $repository, TaskService $taskService)
    {
        $this->repository = $repository;
        $this->taskService = $taskService;
    }

    public function list($where = null, $relations = []): Collection
    {
        return $this->repository->list($where, $relations);
    }

    public function complete(UserTask $userTask)
    {
        $this->repository->complete($userTask->id);
    }

    /**
     * @param  UserTask  $userTask
     *
     * @return UserTask - updated the $userTask
     */
    public function replaceByNew(UserTask $userTask): UserTask
    {
        $newTask = $this->taskService->findNew($userTask->user, $userTask->task->category);

        if ($newTask) {
            $this->repository->update($userTask->id, ['task_id' => $newTask->id]);
        }

        return $userTask->refresh();
    }

    /**
     * Generate a unique selection of tasks for the user
     *
     * @param  User  $user
     * @param  array|int|Collection  $categories
     * @param  \DateTimeInterface  $date
     *
     * @return Collection<UserTask>
     */
    public function generateTaskSelection(User $user, $categories, \DateTimeInterface $date): Collection
    {
        if (is_int($categories)) {
            $categories = [$categories];
        } elseif (is_array($categories)) {
            $categories = collect($categories);
        }

        $tasks = $this->taskService->generateSelection($user, $categories);

        $data = $tasks->map(fn(Task $task) => [
            'user_id' => $user->id,
            'task_id' => $task->id,
            'date'    => $date,
        ])->toArray();

        return $this->createMany($data);
    }

    /**
     * @param  array  $data
     *
     * @return Collection<UserTask>
     */
    public function createMany(array $data): Collection
    {
        return collect($this->repository->createMany($data));
    }
}
