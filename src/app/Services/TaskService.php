<?php

namespace App\Services;

use App\Models\Category;
use App\Models\User;
use App\Repositories\Interfaces\TaskRepositoryInterface;
use Illuminate\Support\Collection;

class TaskService
{
    private TaskRepositoryInterface $repository;

    public function __construct(TaskRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create($data)
    {
        $this->repository->create($data);
    }

    public function list($where = null): Collection
    {
        return $this->repository->list($where);
    }

    public function generateSelection(User $user, $categoryIds)
    {
        return collect($categoryIds)
            ->map(fn($categoryId) => $this->findNew($user, $categoryId));
    }
    /**
     * Find a new task that is not already in user's tasks
     *
     * @param  User  $user
     * @param  Category  $category
     *
     * @return mixed
     */
    public function findNew(User $user, Category $category)
    {
        return $this->repository->findNew($user->id, $category->id);
    }
}
