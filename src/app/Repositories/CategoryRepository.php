<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use Illuminate\Support\Collection;


class CategoryRepository implements CategoryRepositoryInterface
{
    private Category $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function list($where = null): Collection
    {
        return $this->model->where($where)->get();
    }

   public function create(array $data)
   {
       return $this->model->create($data);
   }
}
