<?php

namespace App\Repositories\Interfaces;


interface CategoryRepositoryInterface
{
    public function list($options = null);

    public function create(array $data);
}
