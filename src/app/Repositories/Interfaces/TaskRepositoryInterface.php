<?php

namespace App\Repositories\Interfaces;



interface TaskRepositoryInterface
{
    public function list($options = null);

    public function findNew($userId, $categoryId);

    public function create($data);
}
