<?php

namespace App\Repositories\Interfaces;


interface UserTaskRepositoryInterface
{
    public function list($options = null, $relations = null);

    public function update($id, array $data);

    public function complete($id);

    public function createMany($data);
}
