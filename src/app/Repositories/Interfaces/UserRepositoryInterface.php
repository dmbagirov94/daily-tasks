<?php

namespace App\Repositories\Interfaces;


interface UserRepositoryInterface
{
    public function findBy(array $options);

    public function create(array $data);

    public function list($options = null);
}
