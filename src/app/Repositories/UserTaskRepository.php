<?php

namespace App\Repositories;

use App\Models\UserTask;
use App\Repositories\Interfaces\UserTaskRepositoryInterface;
use Illuminate\Support\Collection;


class UserTaskRepository implements UserTaskRepositoryInterface
{
    private UserTask $model;

    public function __construct(UserTask $model)
    {
        $this->model = $model;
    }

    public function list($where = null, $relations=null): Collection
    {
        return $this->model->where($where)->with($relations)->get();
    }

    public function update($userTaskId, array $data)
    {
        return $this->model->whereId($userTaskId)->update($data);
    }

    public function complete($userTaskId)
    {
        return $this->update($userTaskId, ['completed' => true]);
    }

    public function createMany($data)
    {
        $result = [];

        foreach ($data as $item) {
            $result[] = $this->model->create($item);
        }

        return $result;
    }
}
