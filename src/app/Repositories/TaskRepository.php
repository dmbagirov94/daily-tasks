<?php

namespace App\Repositories;

use App\Models\Task;
use App\Repositories\Interfaces\TaskRepositoryInterface;
use Illuminate\Support\Collection;


class TaskRepository implements TaskRepositoryInterface
{
    private Task $model;

    public function __construct(Task $model)
    {
        $this->model = $model;
    }

    public function list($where = null): Collection
    {
        return $this->model->where($where)->get();
    }

    public function findNew($userId, $categoryId)
    {
        return $this->model
            ->whereCategoryId($categoryId)
            ->whereDoesntHave('userTasks', fn($q) => $q->whereUserId($userId))
            ->first();
    }

    public function create($data)
    {
        return $this->model->create($data);
    }
}
