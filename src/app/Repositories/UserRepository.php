<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Collection;


class UserRepository implements UserRepositoryInterface
{
    private User $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function findBy(array $options): ?User
    {
        return $this->model->where($options)->get();
    }

    public function create(array $data): User
    {
        return $this->model->create($data);
    }

    public function list($where = null): Collection
    {
        return $this->model->where($where)->get();
    }
}
