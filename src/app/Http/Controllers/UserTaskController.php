<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserTaskResource;
use App\Models\User;
use App\Models\UserTask;
use App\Services\UserTaskService;
use Illuminate\Http\Request;

class UserTaskController extends Controller
{
    private UserTaskService $service;
    private ?User $user;

    public function __construct(UserTaskService $service)
    {
        $this->service = $service;
        $this->user = auth()->user();
    }

    public function index()
    {
        $list = $this->service->list(['user_id' => $this->user->id], ['task.category']);

        return UserTaskResource::collection($list);
    }

    public function complete(Request $request, UserTask $userTask)
    {
        $this->service->complete($userTask);

        return response()->noContent();
    }

    public function replace(Request $request, UserTask $userTask)
    {
        $newUserTask = $this->service->replaceByNew($userTask);

        return UserTaskResource::make($newUserTask->load('task.category'));
    }
}
