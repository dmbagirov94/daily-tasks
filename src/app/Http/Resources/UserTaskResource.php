<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserTaskResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'task' => $this->whenLoaded('task'),
            'date' => $this->date,
            'completed' => $this->completed,
        ];
    }
}
