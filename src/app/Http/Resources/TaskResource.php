<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id'  => $this->category_id,
            'title' => $this->title,
            'category' => CategoryResource::make($this->whenLoaded('category')),
        ];
    }
}
