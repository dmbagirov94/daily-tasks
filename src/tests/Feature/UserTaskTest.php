<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Task;
use App\Models\User;
use App\Models\UserTask;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UserTaskTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::create(
            [
                'email'    => 'test@example.com',
                'password' => Hash::make('test123'),
            ]
        );

        $this->actingAs($this->user);
    }

    private function createTestData()
    {
        $category = Category::create(['title' => 'category']);
        $task = Task::create(['title' => 'task', 'category_id' => $category->id]);
        Task::create(['title' => 'task', 'category_id' => $category->id]);
        UserTask::create(['user_id' => $this->user->id, 'task_id' => $task->id, 'date' => now()]);
    }

    public function testIndex()
    {
        $this->createTestData();
        $response = $this->json('GET', '/api/my-tasks');
        $response->assertOk();
        $data = $response->json('data');
        $this->assertCount(1, $data);
    }

    public function testComplete()
    {
        $this->createTestData();
        $response = $this->json('PATCH', '/api/my-tasks/1/complete');
        $response->assertNoContent();
        $this->assertDatabaseHas('user_tasks', ['id' => 1, 'completed' => true]);
    }

    public function testReplace()
    {
        $this->createTestData();
        $oldUserTask = UserTask::whereId(1)->first();
        $response = $this->json('PATCH', '/api/my-tasks/1/replace');
        $response->assertOk();
        $newUserTask = $response->json('data');
        $this->assertEquals($oldUserTask->task->category_id, $newUserTask['task']['category_id']);
        $this->assertNotEquals($oldUserTask->task->id, $newUserTask['task']['id']);
    }
}
