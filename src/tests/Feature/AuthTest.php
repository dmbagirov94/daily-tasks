<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tymon\JWTAuth\JWT;

class AuthTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->jwt = app(JWT::class);

        User::create([
            'email' => 'test@example.com',
            'password' => Hash::make('test123'),
        ]);
    }

    public function testRegistration()
    {
        $response = $this->json('POST','/api/registration', [
            'email' => 'john_doe@example.com',
            'password' => 'test123',
        ]);

        $response->assertOk();
        $data = $response->json();
        $this->assertDatabaseHas('users', ['email' => 'john_doe@example.com']);

        self::assertNotEmpty($data['access_token'] ?? null, 'invalid the "access_token" value');
        self::assertEquals('bearer', $data['token_type'] ?? null,);
        self::assertEquals(config('jwt.ttl') * 60, $data['expires_in'] ?? null);
    }

    public function testLogin()
    {
        $response = $this->json('POST','/api/login', [
            'email' => 'test@example.com',
            'password' => 'test123',
        ]);

        $response->assertOk();
        $data = $response->json();

        self::assertNotEmpty($data['access_token'] ?? null, 'invalid the "access_token" value');
        self::assertEquals('bearer', $data['token_type'] ?? null,);
        self::assertEquals(config('jwt.ttl') * 60, $data['expires_in'] ?? null);
    }
}
