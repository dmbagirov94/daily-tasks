<?php

namespace Tests;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tymon\JWTAuth\JWTAuth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $currentDate = '2021-01-18 12:12:12';
    protected $jwt;
    protected $auth;
    protected $callOptions = [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
    ];

    protected function setUp(): void
    {
        parent::setUp();
        Carbon::setTestNow($this->currentDate);
        $this->auth = app(JWTAuth::class);
        $this->artisan('route:clear');
        $this->artisan('config:clear');
        $this->artisan('migrate:fresh');
    }

    public function actingAs(Authenticatable $user, $driver = null)
    {
        $this->jwt = $this->auth->fromUser($user);

        $this->callOptions = array_filter(array_merge($this->callOptions, [
            'X-CSRF-TOKEN' => null,
            'Authorization' => empty($this->jwt) ? null : "Bearer {$this->jwt}",
        ]));

        return $this;
    }

    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $uri = preg_replace('/\/+/', '/', "/$uri");

        $server = array_merge(
            $this->transformHeadersToServerVars($this->callOptions),
            $server
        );

        $response = parent::call($method, $uri, $parameters, $cookies,
            $files, $server, $content);

        return $response;
    }
}
