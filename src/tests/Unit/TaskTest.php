<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\UserTask;
use App\Services\CategoryService;
use App\Services\TaskService;
use App\Services\UserTaskService;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class TaskTest extends TestCase
{
    private $user;
    private $categories;
    private UserTaskService $userTaskService;
    private CategoryService $categoryService;
    private TaskService $taskService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userTaskService = app(UserTaskService::class);
        $this->taskService = app(TaskService::class);
        $this->categoryService = app(CategoryService::class);
        $this->prepareData();
    }

    private function prepareData()
    {
        $categories = ['math', 'func', 'string', 'booleans'];
        $this->categories = collect($categories)
            ->map(
                function ($category) {
                    $category = $this->categoryService->create(['title' => $category]);

                    for ($i = 1; $i <= 5; $i++) {
                        $this->taskService->create(
                            [
                                'title' => "task №$i for $category->title",
                                'category_id' => $category->id
                            ]
                        );
                    }

                    return $category;
                }
            );

        $this->user = User::create(
            [
                'email' => 'test@example.com',
                'password' => Hash::make('test123'),
            ]
        );
    }

    public function testTaskGeneration()
    {
        $date = now();
        $result = $this->userTaskService->generateTaskSelection($this->user, $this->categories, $date);
        $categories = $result->map(function (UserTask $userTask) {
            assertEquals($this->user->id, $userTask->user_id);

            return $userTask->task->category_id;
        })->toArray();
        $this->assertEquals($this->categories->pluck('id')->toArray(), $categories);
    }
}
