<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    public function run()
    {
        $data = User::factory()
            ->count(1000)
            ->make()
            ->map(fn(User $user) => [
                'email' => $user->email,
                'password' => $user->password
            ])
            ->toArray();
        DB::table('users')->insert($data);
    }
}
