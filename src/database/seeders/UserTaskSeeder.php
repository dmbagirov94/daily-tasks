<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Task;
use App\Models\User;
use App\Models\UserTask;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTaskSeeder extends Seeder
{
    public function run()
    {
        $data = User::all('id')
            ->map(
                fn(User $user) => Task::all()
                    ->map(
                        fn(Task $task) => UserTask::create(
                            [
                                'user_id' => $user->id,
                                'task_id' => $task->id,
                                'date' => '2022-01-18 12:12:12'
                            ]
                        )
                    )
            )
            ->flatten(1)
            ->toArray();
        DB::table('tasks')->insert($data);
    }
}
