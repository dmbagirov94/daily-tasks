<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Task;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class TaskSeeder extends Seeder
{
    public function run()
    {
        $data = Category::all('id')
            ->map(fn(Category $category) => Task::factory(['category_id' => $category->id])
                ->count(4000)
                ->make()
                ->map(fn(Task $task) => Arr::only($task->toArray(), ['title', 'category_id']))
                ->toArray()
            )
            ->flatten(1)
            ->toArray();
        DB::table('tasks')->insert($data);
    }
}
