<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    public function loadCategories()
    {
        $categories = collect(
            [
                'fundamentals',
                'string',
                'algorithms',
                'math',
                'performance',
                'booleans',
                'functions',
            ]
        )
            ->map(fn($title) => ['title' => $title])
            ->toArray();

        DB::table('categories')->insert($categories);
    }

    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('title');
        });

        $this->loadCategories();
    }

    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
