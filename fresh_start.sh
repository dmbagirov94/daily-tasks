#!/bin/sh

docker-compose --env-file=src/.env down -v && \
docker-compose --env-file=src/.env build && \
docker-compose --env-file=src/.env up
