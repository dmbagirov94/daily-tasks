## Daily Tasks

### Run with docker
`` cp .env.example .env``

``sh ./start.sh``

### Run tests with docker
`` cp .env.testing.example .env.testing``

``sh ./tests.sh``


### Run seeders
`` php artisan db:seed --class=UserSeeder``

`` php artisan db:seed --class=TaskSeeder ``

`` php artisan db:seed --class=UserTaskSeeder``
